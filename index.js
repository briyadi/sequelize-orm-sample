const { Articles } = require("./models")
const express = require("express")
const app = express()

async function getArticles(req, res) {
    let response = []
    try {
        response = await Articles.findAll()
        console.log(JSON.stringify(response, null, 2))
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
}

app.set("view engine", "ejs")
app.get("/api/v1/articles", getArticles)
app.get("/", (req, res) => {
    res.render("articles")
})

app.listen(9000, () => console.log(`server started`))